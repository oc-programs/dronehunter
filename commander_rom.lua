mdm = component.proxy(component.list("modem")())
radar = component.proxy(component.list("os_entdetector")())
mdm.setStrength(94) -- 32 under ground plus 42 for robots

function report_heresy()
  mdm.broadcast(0xBABE, "HERESY")
end

function sleep(time)
  waketime = computer.uptime() + time
  while computer.uptime() < waketime do
    computer.pullSignal(10)
  end
end

while true do
  --sleep 1 s
  sleep(1)
  scanned = radar.scanPlayers(32)
  if #scanned>0 then
    for _, v in pairs(scanned) do
      if v.name ~= "man_cubus" then
        report_heresy()
        sleep(50)
      end
    end
  end
end