mdm = component.proxy(component.list("modem")())
red = component.proxy(component.list("redstone")())
r = component.proxy(component.list("robot")())
conf = {"modem_message", "HERESY", "SQUAD_BROKEN"}
height = 42
pull_timeout = 30

mdm.open(0xBABE)

for _ = 1, height do
  r.move(1)
end

active = true

while active do
  msg = {computer.pullSignal(pull_timeout)}
  if msg[1] == conf[1] then
    if msg[6] = conf[2] then
      r.place(0)
      red.setOutput(0,15)
      computer.pullSignal(1)
      red.setOutput(0,0)
    elseif msg[6] = conf[3]
      for _ = 1, height do
        r.move(0)
        active = false
      end
    end
  end
end

computer.shutdown()